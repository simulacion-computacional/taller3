#include <iostream>
#include <vector>
#include "punto.h"
#include "montecarlo.h"

using std::cout;
using std::cin;
using std::endl;
using std::vector;

int main()
{

    vector<double> valoresReales;
    valoresReales.push_back(3.1415);
    valoresReales.push_back(4.1887);
    valoresReales.push_back(4.9348);
    valoresReales.push_back(5.263);
    valoresReales.push_back(5.167);
    valoresReales.push_back(4.72);
    valoresReales.push_back(4.05);
    valoresReales.push_back(3.29);

    MonteCarlo monteCarlo;
    monteCarlo.busquedaHitsParaAreaDada(valoresReales);
    return 0;
}

