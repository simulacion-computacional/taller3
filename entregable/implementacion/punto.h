#ifndef PUNTO_H
#define PUNTO_H

class Punto
{
private:
    double x;
    double y;
public:
    Punto();
    Punto(double x, double y);
    double getX();
    double getY();
    void setX(double x);
    void setY(double y);
    void operator=(Punto otroPunto);
};

#endif // PUNTO_H
