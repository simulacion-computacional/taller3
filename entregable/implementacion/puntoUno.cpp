#include <iostream>
#include "montecarlo.h"

using std::cout;
using std::cin;
using std::endl;

int main()
{
    int dimension;
    cin >> dimension;
    int hits;
    cin >> hits;

    MonteCarlo monteCarlo;

    cout << monteCarlo.areaCirculoN(dimension,hits) << endl;

    return 0;
}
