#include <iostream>
#include <vector>
#include "punto.h"
#include "montecarlo.h"

using std::cout;
using std::cin;
using std::endl;
using std::vector;

int main()
{
    int numeroVertices;
    cin >> numeroVertices;
    int hits;
    cin >> hits;
    vector<Punto> vertices;
    for(int i=0;i<numeroVertices;i++)
    {
        double x;
        cin >> x;
        double y;
        cin >> y;
        Punto p(x,y);
        vertices.push_back(p);
    }
    MonteCarlo monteCarlo;
    cout << monteCarlo.areaPoligono(vertices,hits) << endl;
    return 0;
}
