#ifndef MONTECARLO_H
#define MONTECARLO_H

#include <vector>
#include "punto.h"

class MonteCarlo
{
public:
    MonteCarlo();
    double areaCirculoN(int dimension,int hits);
    double areaPoligono(std::vector<Punto> vertices, int hits);
    std::vector<int> busquedaHitsParaAreaDada(std::vector<double> valorReales);
    int busquedaBinaria(double valorReal, int dimension);

private:
    double min(double x, double y);
    double max(double x, double y);
    Punto puntoMax(std::vector<Punto> vertices);
    bool perteneceCirculoN(std::vector<double> coordenadas);
    bool pertenecePuntoPoligono(std::vector<Punto> vertices, Punto punto);

};
#endif // MONTECARLO_H
